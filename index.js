const animatedObject = document.getElementById('animated-object');

let angleX = 0;
let angleY = 0;
let angleZ = 0;

const animate = function () {
  requestAnimationFrame(animate);

  angleX += 1;
  angleY += 1;
  angleZ += 1;

  animatedObject.style.transform = `rotateX(${angleX}deg) rotateY(${angleY}deg) rotateZ(${angleZ}deg)`;
};

animate();
